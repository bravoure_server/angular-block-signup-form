(function () {
    'use strict';

    function blockSignupForm (PATH_CONFIG, $rootScope, $translate, $location, API_Service, getCredentials) {
        return {
            restrict: "E",
            replace: "true",
            link: function (scope, element, attrs) {

                element.on('active focus', '.form-control', function (e) {
                    e.stopPropagation();
                    $(this).parent().addClass('focused', (this.value.length > 0));
                });

                element.on('blur change', '.form-control', function (e) {
                    e.stopPropagation();

                    $(this).parent().toggleClass('focused', (this.value.length > 0));

                    var focused = $('.focused');

                    focused.each(function (index, obj) {
                        if ($(this).find('select').val() == '?') {
                            $(this).removeClass('focused');
                        }
                    });
                }).trigger('blur');

            },
            controller: function ($scope, $controller) {
                //$scope.login_url = host + 'login?lang=' + PATH_CONFIG.current_language + '&redirect_uri=' + host + 'authorize';

                $scope.showIframe = ($scope.$state.params.identifier == 'top.signup_page') ? true : false;

                $rootScope.$on('hideIframe', function () {
                    $scope.$apply(function () {
                        $scope.showIframe = false;
                    });
                });

                $rootScope.$on('showIframe', function () {
                    $scope.$apply(function () {
                        $scope.showIframe = true;
                    });
                });

                angular.extend(this, $controller('baseController', {
                    $scope: $scope
                }));


                $scope.form_name = 'signup_form';

                $scope.showIframe = ($scope.$state.params.identifier == data.loginIdentifier) ? true : false;

                $rootScope.$on('hideIframe', function () {
                    $scope.$apply(function () {
                        $scope.showIframe = false;
                    });
                });

                $rootScope.$on('showIframe', function () {
                    $scope.$apply(function () {
                        $scope.showIframe = true;
                    });
                });

                // MODEL TO SET DEFAULT VALUES
                $scope.model = {
                    "client_id": getCredentials.byEnv('_client_id'),
                    "client_secret": getCredentials.byEnv('_client_secret'),
                    "grant_type": data.grant_type,
                    "username": ''
                };

                $scope.schema = {
                    "type": "object",
                    "title": "Comment",
                    "properties": {
                        "client_id": {
                            "title": "client_id",
                            "type": "string"
                        },
                        "client_secret": {
                            "title": "client_secret",
                            "type": "string"
                        },
                        "grant_type": {
                            "title": "grant_type",
                            "type": "string"
                        },
                        "username": {
                            "title": "username",
                            "type": "string"
                        },
                        "email":  {
                            "title": $translate.instant('form.email'),
                            "type": "string",
                            "pattern": "^\\S+@\\S+$",
                            "validationMessage": $translate.instant('form.error.email')
                        },
                        "password":  {
                            "title": $translate.instant('form.password'),
                            "type": "string",
                            "minLength": 4,
                            "validationMessage": $translate.instant('form.error.password')
                        },
                        "first_name":  {
                            "title": $translate.instant('form.first_name'),
                            "type": "string",
                            "minLength": 2,
                            "validationMessage": $translate.instant('form.error.first_name')
                        },
                        "last_name":  {
                            "title": $translate.instant('form.last_name'),
                            "type": "string",
                            "validationMessage": $translate.instant('form.error.last_name')
                        }
                    },
                    "required": ["email", "password"]
                };

                $scope.form = [
                    {
                        "key": "client_id",
                        "type": "hidden"
                    },
                    {
                        "key": "client_secret",
                        "type": "hidden"
                    },
                    {
                        "key": "grant_type",
                        "type": "hidden"
                    },
                    {
                        "key": "username",
                        "type": "hidden"
                    },
                    {
                        "key": "email",
                        "type": "email",
                        "required": true
                    },
                    {
                        "key": "password",
                        "type": "password",
                        "required": true
                    },
                    {
                        "key": "first_name"
                    },
                    {
                        "key": "last_name"
                    }
                ];

                if (typeof $scope.module != 'undefined') {

                    $scope.showError = false;
                    $scope.thankyou_show = false;

                }

                $scope.post_url = host + 'api/users';

                $scope.link = $location.path();

                $scope.triggerSubmit = function() {
                    $('.block-newsletter-form__form').trigger('submit');
                };

                $scope.onSubmit = function (form) {

                    form.preventDefault();

                    // First we broadcast an event so all fields validate themselves
                    $scope.$broadcast('schemaFormValidate');

                    // Then we check if the form is valid
                    if ($scope[$scope.form_name].$valid) {

                        var formData = {};

                        angular.forEach($scope.schema.properties, function (value, keys) {
                            formData[keys] = $scope[$scope.form_name][keys].$viewValue;
                        });

                        // TODO - Remove exception to duplicate the value of the email for the username
                        // copies the email value
                        formData.username = formData.email;

                        API_Service($scope.post_url, {}).save_no_array({}, formData,
                            function success(data) {

                                $scope.showError = false;
                                $scope.form_success = true;
                                $scope.thankyou_show = true;

                                $scope.model = {
                                    "client_id": data.dev_client_id,
                                    "client_secret": data.dev_client_secret,
                                    "grant_type": data.grant_type,
                                    "username": ""
                                };

                                $rootScope.$emit ('loggedIn', data);

                            },

                            function error(data) {
                                $scope.showError = true;

                                $scope.error = data.data.errors[0];
                            });
                    }

                };

            },
            templateUrl: PATH_CONFIG.BRAVOURE_COMPONENTS + 'angular-block-signup-form/block-signup-form.html'
        }
    }

    blockSignupForm.$inject = ['PATH_CONFIG', '$rootScope', '$translate', '$location', 'API_Service', 'getCredentials'];

    function getCredentials () {
        return {
            byEnv: function (field) {

                var env_selected;

                // defines the correspondant API
                switch (true) {
                    case host.indexOf("accept") != -1:
                        env_selected = 'accept';
                        break;
                    case host.indexOf("test") != -1:
                        env_selected = 'test';
                        break;
                    case host.indexOf("dev") != -1:
                        env_selected = 'dev';
                        break;
                    default:
                        env_selected = 'master';
                }

                return data[env_selected + field];
            }
        }
    }
    angular
        .module('bravoureAngularApp')
        .factory('getCredentials', getCredentials)
        .directive('blockSignupForm', blockSignupForm);

})();
